#if !defined(__GNUC__)
#include "stdafx.h"
#endif

#include <iostream>
#include <string>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

using namespace std;

const uint64_t ALLBITS = (uint64_t) ~0; // 2^64-1

using Suit = uint8_t;
const Suit hearts = 0, spades = 1, diamonds = 2, clubs = 3;
/// NOTE: if trump is used, it is assumed to be hearts

using Value = uint8_t;
const Value seven = 0, eight = 1, nine = 2, jack = 3, queen = 4, king = 5, ace = 6, manil = 7;

using Score = uint8_t;

/*
 * 32 Cards represented by uint8_t < 32
 * Card x / 4 is the Value
 * Card x % 4 is the Suit
 * e.g. 17 is Q(ueen) of S(pades)
 */
using Card = uint8_t;
const Card heartseven = 0, spadeseven = 1, diamondseven = 2, clubseven = 3;
const Card hearteight = 4, spadeeight = 5, diamondeight = 6, clubeight = 7;
const Card heartnine = 8, spadenine = 9, diamondnine = 10, clubnine = 11;
const Card heartjack = 12, spadejack = 13, diamondjack = 14, clubjack = 15;
const Card heartqueen = 16, spadequeen = 17, diamondqueen = 18, clubqueen = 19;
const Card heartking = 20, spadeking = 21, diamondking = 22, clubking = 23;
const Card heartace = 24, spadeace = 25, diamondace = 26, clubace = 27;
const Card heartmanil = 28, spademanil = 29, diamondmanil = 30, clubmanil = 31;

const string cardstrings[32] = {
    "heartseven", "spadeseven", "diamondseven", "clubseven",
    "hearteight", "spadeeight", "diamondeight", "clubeight",
    "heartnine", "spadenine", "diamondnine", "clubnine",
    "heartjack", "spadejack", "diamondjack", "clubjack",
    "heartqueen", "spadequeen", "diamondqueen", "clubqueen",
    "heartking", "spadeking", "diamondking", "clubking",
    "heartace", "spadeace", "diamondace", "clubace",
    "heartmanil", "spademanil", "diamondmanil", "clubmanil"
};

Card str2card(string &cstr) {
  for (Card c = 0; c < 32; ++c) {
    if (cardstrings[c] == cstr) {
      return c;
    }
  }
  return 32;
}

inline Card getCard(Suit s, Value v) {
  return s + (v << 2);
}

inline Value getValue(Card x) {
  return x >> 2;
}

inline unsigned int getScore(Card x) {
  if (x < 8) { return 0; }
  return (x >> 2) - 2;
}

inline Suit getSuit(Card x) {
  return x % 4;
}

inline bool isTrump(Card x) {
  return getSuit(x) == hearts; // assuming trump is always hearts
}

const string _suit2str[] = {"h", "s", "d", "c"};
const string _val2str[] = {"7", "8", "9", "J", "Q", "K", "A", "X"};

string card2str(const Card c) {
  return "" + _suit2str[getSuit(c)] + _val2str[getValue(c)];
}

/*
 * 32 bits can represent any collection of Cards
 * Then each bit denotes whether the Card is in the collection
 */
using Cards = uint32_t;
const Cards ALLCARDS = (Cards) ~0;

inline bool hasCard(const Cards cs, const Card c) {
  return (bool) ((Cards) cs & (Cards) (1 << c));
}

inline Cards addCard(Cards cs, Card c) {
  return (Cards) cs | (Cards) (1 << c);
}

inline Cards removeCard(Cards cs, Card c) {
  return (Cards) cs & (Cards) ~(1 << c);
}

inline uint8_t getCount(Cards cs) {
#if defined(__GNUC__)
  // use g++ specific optimization
  // NOTE: equivalent optimizations exist on other platforms
  return __builtin_popcount(cs);
#endif
  uint8_t out = 0;
  for (Card c = 0; c < 32; ++c) {
    out += hasCard(cs, c);
  }
  return out;
}

string cards2str(const Cards cs, bool longFormat = true) {
  string result = "";
  if (longFormat) {
    for (int j = 0; j < 4; ++j) {
      for (Card i = j; i < 32; i += 4) {
        if (hasCard(cs, i)) {
          result += card2str(i) + " ";
        }
      }
    }
  } else {
    for (int j = 0; j < 4; ++j) {
      result += _suit2str[j];
      for (Card i = j; i < 32; i += 4) {
        if (hasCard(cs, i)) {
          result += _val2str[i / 4];
        } else {
          result += " ";
        }
      }
    }
  }
  return result;
}

inline Card getLowest(Cards cs) {
#if defined(__GNUC__)
  // use g++ specific optimization
  // NOTE: equivalent optimizations exist on other platforms
  return __builtin_ctz(cs);
#endif
  for (Card c = 0; c < 32; ++c) {
    if (!hasCard(cs, c)) continue;
    return c;
  }
  assert(false);
  return 32;
}

inline Card getHighest(Cards cs) {
#if defined(__GNUC__)
  // use g++ specific optimization
  // NOTE: equivalent optimizations exist on other platforms
  return 31-__builtin_clz(cs);
#endif
  for (Card c = 0; c < 32; ++c) {
    Card result = 31 - c;
    if (!hasCard(cs, result)) continue;
    return result;
  }
  assert(false);
  return 32;
}

inline Cards merge(Cards cs1, Cards cs2) {
  return (Cards) cs1 | (Cards) cs2;
}

inline Cards intersect(Cards cs1, Cards cs2) {
  return (Cards) cs1 & (Cards) cs2;
}

inline Cards setminus(Cards cs1, Cards cs2) {
  return (Cards) cs1 & (Cards) ~cs2;
}

bool isEmpty(Cards cs) {
  return cs == (Cards) 0;
}

const Cards SUITED = 286331153; // 10001000100010001000100010001
inline Cards getCardsOfSuit(Cards cs, Suit s) {
  return (Cards) cs & (Cards) (SUITED << s);
}

inline Cards getCardsOfSameSuit(Cards cs, Card c) {
  return getCardsOfSuit(cs, getSuit(c));
}

inline Card getLowestCardOfSuit(Cards cs, Suit s) {
#if defined(__GNUC__)
  return getLowest(getCardsOfSuit(cs,s));
#endif
  for (Card c = s; c < 32; c+=4) {
    if (!hasCard(cs, c)) continue;
    return c;
  }
  assert(false);
  return 32;
}

inline Card getHighestCardOfSuit(Cards cs, Suit s) {
#if defined(__GNUC__)
  return getHighest(getCardsOfSuit(cs, s));
#endif
  for (int i = 28+s; i >=0; i-=4) {
    Card c = i;
    if (!hasCard(cs, c)) continue;
    return c;
  }
  assert(false);
  return 32;
}

inline Cards getTrumpCards(Cards cs, bool trumpUsed) {
  if (trumpUsed) return getCardsOfSuit(cs, hearts);
  return 0;
}

inline Cards getLowerCardsThan(Cards cs, Card c) {
  return (Cards) cs & (Cards) (ALLCARDS >> (32 - c)); // excludes c
}

inline Cards getHigherCardsThan(Cards cs, Card c) {
  return (Cards) cs & (Cards) (ALLCARDS << (1 + c)); // excludes c
}

inline Cards getLowerCardsOfSameSuit(Cards cs, Card c) {
  return getLowerCardsThan(getCardsOfSameSuit(cs, c), c);
}

inline Cards getHigherCardsOfSameSuit(Cards cs, Card c) {
  return getHigherCardsThan(getCardsOfSameSuit(cs, c), c);
}

inline Cards getEveryCardExceptLowerOfSuit(Cards cs, Card c) {
  return setminus(cs, getLowerCardsOfSameSuit(cs, c));
}

inline Cards swapSuitWithTrumps(Cards in, Suit suit) {
  Cards trumpCards = getCardsOfSuit(in, hearts);
  Cards suitCards = getCardsOfSuit(in, suit);
  return merge(setminus(in, merge(suitCards, trumpCards)), merge(suitCards >> suit, trumpCards << suit));
}

inline Card swapCardSuitWithTrumps(Card in, Suit suit){
  if(getSuit(in)==suit){
    return getCard(hearts,getValue(in));
  }else if(getSuit(in)==hearts){
    return getCard(suit,getValue(in));
  }else{
    return in;
  }
}

inline bool isBetterCardThan(Card c1, Card c2, bool trumpused) {
  if (getSuit(c1) == getSuit(c2)) {
    return getValue(c1) > getValue(c2);
  }
  return trumpused && isTrump(c1); // NOTE: getSuit(c1)!=getSuit(c2), so both cannot be trump
}

/**
 * 1) van jouw team, niet gekocht
 * -> speel elke kleurkaart | speel elke kaart
 * 2) van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke kaart behalve lagere troef | speel elke kaart
 * 3) niet van jouw team, niet gekocht
 * -> speel elke hogere kleurkaart | speel elke kleurkaart | speel elke troef | speel elke kaart
 * 4) niet van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke hogere troef | speel elke kaart behalve lagere troef | speel elke kaart
 **/
inline Cards getPlayableCards(Cards cs, Card initial, Card best, bool teamleads, bool trumpused) {
  assert(!hasCard(cs, initial));
  assert(!hasCard(cs, best));
  assert(!isBetterCardThan(initial, best, trumpused));

  bool bought = trumpused && isTrump(best) && !isTrump(initial);
  Cards initialSuited = getCardsOfSameSuit(cs, initial);
  if (teamleads) { // your team has the best Card
    if (initialSuited) return initialSuited; // speel elke kleurkaart
    if (bought) {
      Cards notUnderBuying = getEveryCardExceptLowerOfSuit(cs, best);
      if (notUnderBuying) return notUnderBuying; // speel elke kaart behalve lagere troef
    }
    return cs; // speel elke kaart
  } else {
    if (bought) {
      if (initialSuited) return initialSuited; // speel elke kleurkaart
      Cards higherTrump = getHigherCardsOfSameSuit(cs, best);
      if (higherTrump) return higherTrump; // speel elke hogere troef
      Cards notUnderBuying = getEveryCardExceptLowerOfSuit(cs, best);
      if (notUnderBuying) return notUnderBuying; // speel elke kaart behalve lagere troef
    } else {
      Cards higherBest = getHigherCardsOfSameSuit(cs, best);
      if (higherBest) return higherBest; // speel elke hogere kleurkaart
      if (initialSuited) return initialSuited; // speel elke kleurkaart
      Cards trumpSuited = getTrumpCards(cs, trumpused);
      if (trumpSuited) return trumpSuited; // speel elke troef
    }
    return cs; // speel elke kaart
  }
}

Cards getCards(vector<Card> cs) {
  Cards result = 0;
  for (auto c: cs) {
    result = addCard(result, c);
  }
  return result;
}

vector<Card> getVector(Cards cs) {
  vector<Card> result;
  result.reserve(8);
  for (Value j = 0; j < 8; ++j) {
    for (Suit i = 0; i < 4; ++i) {
      Card c = i * 8 + j;
      if (hasCard(cs, c)) {
        result.push_back(c);
      }
    }
  }
  return result;
}

void testGetPlayableCards() {
  Cards input, output;
/**
 * 1) van jouw team, niet gekocht
 * -> speel elke kleurkaart | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil}), clubnine, clubnine, true, true);
  output = getCards({clubeight, clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil}), diamondnine, diamondnine, true,
                           true);
  output = getCards({clubeight, clubjack, clubmanil, heartmanil});
  assert(input == output);
/**
 * 2) van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke kaart behalve lagere troef | speel elke kaart
 **/

  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, heartjack, true, true);
  output = getCards({clubeight, clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, heartjack, true, true);
  output = getCards({heartmanil, diamondace, spadeking});
  assert(input == output);
  input = getPlayableCards(getCards({heartnine}),
                           clubnine, heartjack, true, true);
  output = getCards({heartnine});
  assert(input == output);
/**
 * 3) niet van jouw team, niet gekocht
 * -> speel elke hogere kleurkaart | speel elke kleurkaart | speel elke troef | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, true);
  output = getCards({clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, true);
  output = getCards({clubeight});
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, true);
  output = getCards({heartmanil, heartnine});
  assert(input == output);
  input = getPlayableCards(getCards({diamondace, spadeking}),
                           clubnine, clubnine, false, true);
  output = getCards({diamondace, spadeking});
  assert(input == output);
/**
 * 4) niet van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke hogere troef | speel elke kaart behalve lagere troef | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, heartjack, false, true);
  output = getCards({clubeight, clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, heartjack, false, true);
  output = getCards({heartmanil});
  assert(input == output);
  input = getPlayableCards(getCards({heartnine, diamondace, spadeking}),
                           clubnine, heartjack, false, true);
  output = getCards({diamondace, spadeking});
  assert(input == output);
  input = getPlayableCards(getCards({heartnine}),
                           clubnine, heartjack, false, true);
  output = getCards({heartnine});
  assert(input == output);

  // NOTE: test previous situations, but now no trump is used

/**
 * 1) van jouw team, niet gekocht
 * -> speel elke kleurkaart | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil}),
                           clubnine, clubnine, true, false);
  output = getCards({clubeight, clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil}),
                           diamondnine, diamondnine, true, false);
  output = getCards({clubeight, clubjack, clubmanil, heartmanil});
  assert(input == output);
/**
 * 2) van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke kaart behalve lagere troef | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubqueen, true, false);
  output = getCards({clubeight, clubjack, clubmanil});
  // NOTE: second case can not happen, as no trump
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubqueen, true, false);
  output = getCards({heartmanil, heartnine, diamondace, spadeking});
  assert(input == output);
/**
 * 3) niet van jouw team, niet gekocht
 * -> speel elke hogere kleurkaart | speel elke kleurkaart | speel elke troef | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, false);
  output = getCards({clubjack, clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, false);
  output = getCards({clubeight});
  // NOTE: third case can not happen, as no trump
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubnine, false, false);
  output = getCards({heartmanil, heartnine, diamondace, spadeking});
  assert(input == output);
/**
 * 4) niet van jouw team, gekocht
 * -> speel elke kleurkaart | speel elke hogere troef | speel elke kaart behalve lagere troef | speel elke kaart
 **/
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubqueen, false, false);
  output = getCards({clubmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubqueen, false, false);
  output = getCards({clubeight, clubjack});
  // NOTE: second case can not happen, as no trump
  // NOTE: third case can not happen, as no trump
  assert(input == output);
  input = getPlayableCards(getCards({heartmanil, heartnine, diamondace, spadeking}),
                           clubnine, clubqueen, false, false);
  output = getCards({heartmanil, heartnine, diamondace, spadeking});

  // extra tests: trump is played, can you follow
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           heartjack, heartking, false, true);
  output = getCards({heartmanil});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartnine, diamondace, spadeking}),
                           heartjack, heartking, false, true);
  output = getCards({heartnine});
  assert(input == output);
  input = getPlayableCards(getCards({clubeight, clubjack, clubmanil, heartmanil, heartnine, diamondace, spadeking}),
                           heartjack, heartking, true, true);
  output = getCards({heartmanil, heartnine});
  assert(input == output);
}

uint64_t inline getBits(uint64_t source, uint8_t start, uint8_t size) {
  return (source >> start) & ~(ALLBITS << size);
}

uint64_t inline setBits(uint64_t source, uint8_t start, uint8_t size, uint64_t value) {
  return (source & ~(~(ALLBITS << size) << start)) | (value << start);
}

using Player = uint8_t;

inline Player nextPlayer(Player p) {
  return (p + 1) % 4;
}

using Team = bool;

inline bool getTeam(Player p) {
  return p % 2;
}

inline bool inverseTeam(Team t) {
  return !t;
}

struct State {
private:
  // TODO: compress this info to 3 uint64_t numbers
  /*
   * Current hands of players
   */
  Cards p[4];
  /*
   * Currently played cards by players
   */
  Card c[4];
  /*
   * Starting bit count from the right:
   * 0-1 : startingPlayer
   * 2-3 : currentPlayer
   * 4-5 : leadingPlayer
   * 6-11 : scoreWon (by team 0)
   * 12-17 : scoreLost (by team 0)
   * 22 : teamThatWinsHand
   * 23 : teamThatWinsHandIsSet
   * 31 : trumpUsed // NOTE: getTrumpUsed() exploits that trumpUsed uses the rightmost bit in a 32-bit word
   */
  uint32_t info = 0;

  inline void setTrumpUsed(bool trumpUsed) { info = setBits(info, 31, 1, trumpUsed); }
  inline void setStartingPlayer(Player sp) {
    assert(sp < 4);
    info = setBits(info, 0, 2, sp);
  }
  inline void setCurrentPlayer(Player current) {
    assert(current < 4);
    info = setBits(info, 2, 2, current);
  }
  inline void increaseCurrentPlayer() { info = setBits(info, 2, 2, nextPlayer(getCurrentPlayer())); }
  inline void setLeadingPlayer(Player leading) {
    assert(leading < 4);
    info = setBits(info, 4, 2, leading);
  }
  inline void increaseScoreWon(Score score) {
    assert(score < 61);
    info = setBits(info, 6, 6, getScoreWon() + score);
  }
  inline void increaseScoreLost(Score score) {
    assert(score < 61);
    info = setBits(info, 12, 6, getScoreLost() + score);
  }
  inline void setPlayerHand(Player i, Cards cs) {
    assert(i < 4);
    p[i] = cs;
  }
  inline void setPlayerCard(Player i, Card crd) {
    assert(i < 4);
    c[i] = crd;
  }

public:

  inline void resetTeamThatWinsHand() { info = setBits(info, 23, 1, 0); }
  inline void setTeamThatWinsHand(Team t) {
    info = setBits(info, 22, 2, (uint64_t) 2 | (uint64_t) t); // set both bit 22 and 23 at the same time
  }
  inline Team getTeamThatWinsHand() const {
    return getBits(info, 22, 1) != 0;
  }
  inline bool teamThatWinsHandIsSet() const {
    return getBits(info, 23, 1) != 0;
  }

  State(Cards hands[4], Card played[4], bool trumpUsed, Player starting, Player current){
    for(Player p=0; p<4; ++p){
      setPlayerHand(p,hands[p]);
      setPlayerCard(p,played[p]);
    }
    setStartingPlayer(starting);
    setCurrentPlayer(current);

    // initialize leading player
    Player leading = starting;
    Player checked = starting;
    while(checked!=current){
      if(isBetterCardThan(getPlayerCard(checked),getPlayerCard(leading),trumpUsed)){
        leading = checked;
      }
      checked = nextPlayer(checked);
    }
    setLeadingPlayer(leading);

    // initialize trumpUsed
    setTrumpUsed(trumpUsed && !isEmpty(getCardsOfSuit(getCardsInGame(),hearts)));
  }

  inline bool getTrumpUsed() const { return info >= (1<<31); }
  inline Player getStartingPlayer() const { return getBits(info, 0, 2); }
  inline Player getCurrentPlayer() const { return getBits(info, 2, 2); }
  inline Team getCurrentTeam() const { return getTeam(getCurrentPlayer()); }
  inline Player getLeadingPlayer() const { return getBits(info, 4, 2); }
  inline Team getLeadingTeam() const { return getTeam(getLeadingPlayer()); }
  inline bool getCurrentPlayerTeamLeads() const { return getLeadingTeam() == getCurrentTeam(); }
  inline Score getScoreWon() const { return getBits(info, 6, 6); }
  inline Score getScoreLost() const { return getBits(info, 12, 6); }
  inline Cards getPlayerHand(Player i) const {
    assert(i < 4);
    return p[i];
  }
  inline Card getPlayerCard(Player i) const {
    assert(i < 4);
    return c[i];
  }

  inline Team getTeamThatWonPreviousHand() const {
    return getTeam(getStartingPlayer());
  }

  inline Card getLeadingCard() const { return getPlayerCard(getLeadingPlayer()); }

  inline Card getInitialCard() const { return getPlayerCard(getStartingPlayer()); }

  inline bool isAtStartOfRound() const {
    return getCurrentPlayer() == getStartingPlayer();
  }

  inline Cards getCardsInGame() const {
    return merge(merge(getPlayerHand(0), getPlayerHand(1)), merge(getPlayerHand(2), getPlayerHand(3)));
  }

  inline Score getPlayedScore() const {
    Score result = 0;
    for (Player i = getStartingPlayer(); i != getCurrentPlayer(); i = nextPlayer(i)) {
      result += getScore(getPlayerCard(i));
    }
    return result;
  }

  inline Score getTotalPlayedScore() const {
    Score result = 0;
    for (Player i = 0; i < 4; ++i) {
      result += getScore(getPlayerCard(i));
    }
    return result;
  }

  inline Cards getCurrentlyPlayableCards() const {
    Cards currentHand = getPlayerHand(getCurrentPlayer());
    if (isAtStartOfRound()) {
      return currentHand; // starting player can play any Card
    } else { // non-startingplayer is restricted depending on state
      return getPlayableCards(currentHand, getInitialCard(), getLeadingCard(), getCurrentPlayerTeamLeads(),
                              getTrumpUsed());
    }
  }

  inline void playCard(Card toPlay) { // TODO: make this a const method by returning a copy
    assert(hasCard(getPlayerHand(getCurrentPlayer()), toPlay));
    assert(hasCard(getCurrentlyPlayableCards(), toPlay));
    // set the Card as played
    setPlayerCard(getCurrentPlayer(), toPlay);

    // figure out which player takes the hand
    // starting player takes the hand and non-starting player has to beat best Card to lead the hand
    if (isAtStartOfRound() || isBetterCardThan(toPlay, getLeadingCard(), getTrumpUsed())) {
      setLeadingPlayer(getCurrentPlayer());
    }

    // onto the next player
    increaseCurrentPlayer();

    if (isAtStartOfRound()) { // round is over, wrap up
      for (Player p = 0; p < 4; ++p) {
        // remove the played card
        setPlayerHand(p, removeCard(getPlayerHand(p), getPlayerCard(p)));
      }
      setTrumpUsed(getTrumpUsed() && !isEmpty(getCardsOfSuit(getCardsInGame(),hearts)));
      Player leadingPlayer = getLeadingPlayer();
      if (getTeam(leadingPlayer) == 0) {
        increaseScoreWon(getTotalPlayedScore());
      } else {
        increaseScoreLost(getTotalPlayedScore());
      }
      setStartingPlayer(leadingPlayer);
      setCurrentPlayer(leadingPlayer);
      resetTeamThatWinsHand();
    }
  }

  inline bool hasPlayed(Player player) const {
    return (getStartingPlayer() < getCurrentPlayer() && player >= getStartingPlayer() && player < getCurrentPlayer())
           || (getStartingPlayer() >= getCurrentPlayer() &&
               (player >= getStartingPlayer() || player < getCurrentPlayer()));
  }

  inline int getScoreDiff() const {
    return getScoreWon() - getScoreLost();
  }

  inline Score getScoreLeft() const {
    assert(60 >= getScoreWon() + getScoreLost());
    return 60 - getScoreWon() - getScoreLost();
  }

  inline string toString() const {
    string result = "";
    result += "trump: " + to_string(getTrumpUsed()) + " scorediff: " + to_string(getScoreDiff()) +
              (isAtStartOfRound() ? " round ended" : "") + "\n";
    for (int pl = 0; pl < 4; ++pl) {
      result += cards2str(getPlayerHand(pl), false) + (getStartingPlayer() == pl ? "<" : " ") +
                (getLeadingPlayer() == pl ? ">" : " ") + " " + (hasPlayed(pl) ? card2str(getPlayerCard(pl)) : "   ") +
                "\n";
    }
    return result;
  }

  inline bool startingTeamLosesAll() const {
    assert(isAtStartOfRound());

    Player starting = getStartingPlayer();
    Cards startinghand = getPlayerHand(starting);
    Cards friendhand = getPlayerHand(nextPlayer(nextPlayer(starting)));
    Cards enemyhand1 = getPlayerHand(nextPlayer(starting));
    Cards enemyhand2 = getPlayerHand(nextPlayer(nextPlayer(nextPlayer(starting))));
    Cards allied = merge(startinghand,friendhand);
    Cards enemy = merge(enemyhand1,enemyhand2);

    if(getTrumpUsed()){
      if (!isEmpty(getCardsOfSuit(allied,hearts))){
        return false; // a trump in allied cards may take a trick
      }
      enemy = setminus(enemy,getCardsOfSuit(enemy,hearts)); // no need to take account of low trumps
      if(isEmpty(enemy)){
        return true; // only trump in enemy's cards
      }
    }

    if(getLowest(enemy)<getHighest(allied)) {
      return false; // some cards still win
    }
    for(Suit s=0; s<4; ++s){
      if(!isEmpty(getCardsOfSuit(startinghand,s)) && isEmpty(getCardsOfSuit(enemy,s))){
        return false; // missing suit wins
      }
    }
    return true;
  }

  inline bool startingTeamWinsAll() const {
    assert(isAtStartOfRound());

    Player starting = getStartingPlayer();
    Cards startinghand = getPlayerHand(starting);
    Cards friendhand = getPlayerHand(nextPlayer(nextPlayer(starting)));
    Cards enemyhand1 = getPlayerHand(nextPlayer(starting));
    Cards enemyhand2 = getPlayerHand(nextPlayer(nextPlayer(nextPlayer(starting))));
    Cards allied = merge(startinghand,friendhand);
    Cards enemy = merge(enemyhand1,enemyhand2);

    if(getTrumpUsed()){
      if(isEmpty(getCardsOfSuit(enemy,hearts)) && getLowest(setminus(allied,getCardsOfSuit(allied,hearts)))>getHighest(enemy)){
        return true; // we have all trumps, remaining cards are only winning
      }
    }else if(getLowest(allied)>getHighest(enemy)){
      return true; // only winning cards
    }

    for (Card i = 0; i < 32; ++i) {
      Card c = 31 - i;
      if (!hasCard(startinghand, c)) continue;
      if (!isEmpty(getHigherCardsOfSameSuit(merge(friendhand, merge(enemyhand1, enemyhand2)), c))) {
        return false;
      }else{
        Suit s = getSuit(c);
        if(!isEmpty(getCardsOfSuit(friendhand,s))) friendhand = removeCard(friendhand,getHighestCardOfSuit(friendhand, s));
        if(!isEmpty(getCardsOfSuit(enemyhand1,s))) enemyhand1 = removeCard(enemyhand1,getLowestCardOfSuit(enemyhand1, s));
        if(!isEmpty(getCardsOfSuit(enemyhand2,s))) enemyhand2 = removeCard(enemyhand2,getLowestCardOfSuit(enemyhand2, s));
      }
    }
    if (getTrumpUsed() && !isEmpty(getCardsOfSuit(merge(enemyhand1,enemyhand2), hearts))) {
      // NOTE: if friend still has trump, he can keep them until end of play and take all remaining tricks
      return false;
    }
    return true;
  }

  inline bool currentTeamAlwaysWins(){
    assert(!isAtStartOfRound());
    Cards toPlay = getCurrentlyPlayableCards();
    if(nextPlayer(getCurrentPlayer())==getStartingPlayer()){ // base case of recursive method
      return (getLeadingTeam()==getCurrentTeam()) || isBetterCardThan(getHighest(toPlay), getLeadingCard(), getTrumpUsed());
    }
    for(Suit s=0; s<4; ++s){
      Cards suited = getCardsOfSuit(toPlay,s);
      if(isEmpty(suited)) continue;
      Card c = getLowestCardOfSuit(suited,s);
      State clone = *this;
      clone.playCard(c);
      if(!clone.currentTeamAlwaysLoses()){
        return false;
      }
    }
    return true;
  }

  inline bool currentTeamAlwaysLoses(){
    assert(!isAtStartOfRound());
    Cards toPlay = getCurrentlyPlayableCards();
    if(nextPlayer(getCurrentPlayer())==getStartingPlayer()){ // base case of recursive method
      return (getLeadingTeam()!=getCurrentTeam()) && !isBetterCardThan(getHighest(toPlay), getLeadingCard(), getTrumpUsed());
    }
    for(Suit s=0; s<4; ++s){
      Cards suited = getCardsOfSuit(toPlay,s);
      if(isEmpty(suited)) continue;
      Card c = getHighestCardOfSuit(suited,s);
      State clone = *this;
      clone.playCard(c);
      if(!clone.currentTeamAlwaysWins()){
        return false;
      }
    }
    return true;
  }
};

int64_t TOTALNODES = 0;
clock_t BEGIN = clock();

inline bool hasBetterCard(Cards hand, Card starting, Card leading, bool teamLeads, bool trumpUsed){
  return isBetterCardThan(getHighest(getPlayableCards(hand,starting,leading,teamLeads,trumpUsed)), leading, trumpUsed);
}

inline Cards cleanSevenEightNines(Cards cs, Suit s, Cards leftInGame){
  Card zeven = getCard(s, seven);
  Card acht = getCard(s, eight);
  if (hasCard(cs, acht)) {
    cs = removeCard(cs, zeven);
  }
  Card negen = getCard(s, nine);
  if (hasCard(cs, negen)) {
    cs = removeCard(cs, acht);
    if (!hasCard(leftInGame, acht)) {
      cs = removeCard(cs, zeven);
    }
  }
  return cs;
}

struct DominatingCards{
  Cards cleaned = 0;
  Cards winning = 0;
  Cards losing = 0;
};

DominatingCards cleanDominatedCardsSimple(Cards cs, State &game, bool downwards) {
  Cards leftInGame = game.getCardsInGame();
  // NOTE: leftInGame should include the cards currently played!
  // This is achieved by only removing played cards from hands at the end of a round (see playCard() )
  for (Suit s = 0; s < 4; ++s) { // iterate over all suits
    if(isEmpty(getCardsOfSuit(cs,s))) continue;
    bool hasDominated = false;
    for(Card i=0;i<32; i+=4){
      Card c = downwards?28+s-i:s+i;
      if(!hasCard(leftInGame,c)) continue;
      if(hasCard(cs,c)){
        if(hasDominated){ // passed dominating card, so erase it
          cs = removeCard(cs,c);
        }else{
          hasDominated = true; // this is a dominating card
        }
      }else{
        hasDominated = false; // a gap in cs, so need to find a new dominating card
      }
    }
  }
  assert(!isEmpty(cs));
  if(downwards){
    return {cs,cs,0};
  }else{
    return {cs,0,cs};
  }
}

DominatingCards cleanDominatedCards(Cards cs, State &game){ // stronger version of symmetry breaking
  if(game.teamThatWinsHandIsSet()){
    return cleanDominatedCardsSimple(cs, game, game.getCurrentTeam() == game.getTeamThatWinsHand());
  }
  if(nextPlayer(game.getCurrentPlayer())==game.getStartingPlayer()){
    bool currentTeamLeads = (game.getLeadingTeam() == game.getCurrentTeam());
    bool currentTeamWins = currentTeamLeads || isBetterCardThan(getHighest(cs), game.getLeadingCard(), game.getTrumpUsed());
    return cleanDominatedCardsSimple(cs, game, currentTeamWins);
  }

  Cards leftInGame = game.getCardsInGame();
  Cards winning = 0;
  Cards losing = 0;
  DominatingCards result;
  // NOTE: leftInGame should include the cards currently played!
  // This is achieved by only removing played cards from hands at the end of a round (see playCard() )
  for (Suit s = 0; s < 4; ++s) { // iterate over all suits
    if(isEmpty(getCardsOfSuit(cs,s))) continue;
    bool hasDominated = false;
    for(int v=7;v>=0; --v){
      Card c = getCard(s,v);
      if(!hasCard(leftInGame,c)) continue;
      if(hasCard(cs,c)){
        if(hasDominated){ // passed dominating card, so erase it
          cs = removeCard(cs,c);
        }else{
          State clone = game;
          clone.playCard(c);
          hasDominated = clone.currentTeamAlwaysLoses();
          if(hasDominated){
            winning=addCard(winning,c);
          }else{
            break; // no dominating card exists
          }
        }
      }else{
        hasDominated = false; // a gap in cs, so need to find a new dominating card
      }
    }
    hasDominated = false;
    for(int v=0;v<8; ++v){
      Card c = getCard(s,v);
      if(!hasCard(leftInGame,c)) continue;
      if(hasCard(cs,c)){
        if(hasDominated){ // passed dominating card, so erase it
          cs = removeCard(cs,c);
        }else{
          State clone = game;
          clone.playCard(c);
          hasDominated = clone.currentTeamAlwaysWins();
          if(hasDominated){
            losing = addCard(losing,c);
          }else{
            break; // no dominating card exists
          }
        }
      }else{
        hasDominated = false; // a gap in cs, so need to find a new dominating card
      }
    }
    cs = cleanSevenEightNines(cs,s,leftInGame);
  }
  assert(!isEmpty(cs));
  return {cs, intersect(cs,winning), intersect(cs,losing)};
  return result;
}


/**
 * NOTE: to work properly, it is best that the highest card in an equivalence class is the representative of that
 * equivalence class. This ensures the score of the equivalence class (which is the highest possible score for a card in
 * that class) is used in heuristic algorithms, such as the one below.
 */
// TODO: keep thinking about this
Card getHeuristicBestCard(DominatingCards& dcs, State &node) {
  Cards cs = dcs.cleaned;
  if (nextPlayer(node.getCurrentPlayer())==node.getStartingPlayer()) { // node is at end of round
    if (node.getLeadingTeam() == node.getCurrentTeam()) {
      return getHighest(cs); // if team member leads, increase his score
    } else {
      // else, play worst card (which is also good if it takes the hand, as then one keeps the better cards to play later)
      return getLowest(cs);
    }
  } else { // node is second or third player of round
    if(!isEmpty(dcs.winning)){
      return getHighest(dcs.winning);
    }
    if(cs==dcs.losing){
      return getLowest(dcs.losing);
    }
    Cards notLosing = setminus(cs,dcs.losing);
    assert(!isEmpty(notLosing));
    if(node.getLeadingTeam() == node.getCurrentTeam()){
      return getHighest(notLosing);
    }
    return getLowest(notLosing);
  }
}

int pvs(State &node, int alpha, int beta) {
  ++TOTALNODES;
  const int left = node.getScoreLeft();
  const int diff = node.getScoreDiff();

  if (left <= 0) {
    return diff;
  }
  bool maximizingNode = node.getCurrentTeam() == 0;
  if (node.isAtStartOfRound()){
    if(node.startingTeamLosesAll()) {
      return maximizingNode ? diff - left : diff + left;
    } else if (node.startingTeamWinsAll()) {
      return maximizingNode ? diff + left : diff - left;
    }
  }
  alpha = max(alpha, diff - left); // alpha is at least the minimum obtainable score
  beta = min(beta, diff + left); // beta is at most the maximum obtainable score

  // only get dominating cards
  DominatingCards domcards = cleanDominatedCards(node.getCurrentlyPlayableCards(), node);
  assert(!isEmpty(domcards.cleaned));

  int v = -1000;
  while (!isEmpty(domcards.cleaned) && alpha < beta) { // alpha-beta pruning
    State child = node;
    Card c = getHeuristicBestCard(domcards, child);
    if(!child.teamThatWinsHandIsSet()){
      if(hasCard(domcards.winning,c)){
        child.setTeamThatWinsHand(child.getCurrentTeam());
      }else if(hasCard(domcards.losing,c)){
        child.setTeamThatWinsHand(!child.getCurrentTeam());
      }
    }
    domcards.cleaned = removeCard(domcards.cleaned, c);
    domcards.winning = removeCard(domcards.winning, c);
    domcards.losing = removeCard(domcards.losing, c);
    child.playCard(c);
    if (v != -1000) { // non-principal variation, do a null-window search
      if (maximizingNode) {
        v = pvs(child, alpha, alpha + 1);
      } else {
        v = pvs(child, beta - 1, beta);
      }
    }
    if (v == -1000 || (alpha < v && v < beta)) { // principal variation or better v's might exist, do a full search
      v = pvs(child, alpha, beta);
    }
    if (maximizingNode) {
      alpha = max(alpha, v);
    } else {
      beta = min(beta, v);
    }
  }
  return maximizingNode ? alpha : beta;
}

int getTrumpScore(Cards cs) {
  int out = 0;
  for (Card c = 0; c < 32; ++c) {
    if (hasCard(cs, c)) {
      out += 50 + getValue(c) * getValue(c);
    }
  }
  return out;
}

Suit getReasonableTrumpSuit(Cards cs) {
  Suit bestSoFar = 0;
  int trumpscore = 0;
  Cards suited = 0;
  for (Suit s = 0; s < 4; ++s) {
    suited = getCardsOfSuit(cs, s);
    int tmptrumpscore = getTrumpScore(suited);
    if (tmptrumpscore > trumpscore) {
      bestSoFar = s;
      trumpscore = tmptrumpscore;
    }
  }
  return bestSoFar;
}

bool goNoTrump(Cards cs) {
  return getTrumpScore(cs) > 600;
}

uint64_t SEED = 1103515245;
int myrandom(int i){
  SEED ^= (SEED << 21);
  SEED ^= (SEED >> 35);
  SEED ^= (SEED << 4);
  return SEED%i;
}

State createRandomGame(int64_t seed){
  SEED = seed;
  vector<Card> allcards = getVector(ALLCARDS);
  random_shuffle(allcards.begin(), allcards.end(),myrandom);
  vector<Card> eightcards;
  eightcards.resize(8);
  Card played[4] = {0,0,0,0};
  Cards hands[4];
  for (Player i = 0; i < 4; ++i) {
    for (uint8_t j = 0; j < 8; ++j) {
      eightcards[j] = allcards[i * 8 + j];
    }
    hands[i]=getCards(eightcards);
  }
  return State(hands,played,false,1,1);
}

State createRandomGameWithTrumpDecided(int64_t seed) {
  // aims to create a random game with reasonable trump/notrump choices
  State game = createRandomGame(seed);
  // picking best player to go notrump increases notrump chance
  // as this is probably computationally more heavy, and thus important to optimize for
  Card played[4] = {0,0,0,0};
  Cards hands[4];
  bool useTrump = true;
  for (Player p = 0; p < 4; ++p) {
    if (useTrump && goNoTrump(game.getPlayerHand(p))) {
      useTrump = false;
      hands[0] = game.getPlayerHand(p);
      hands[p] = game.getPlayerHand(0);
    } else {
      hands[p]=game.getPlayerHand(p);
    }
  }
  if(useTrump){
    Suit trump = getReasonableTrumpSuit(game.getPlayerHand(0));
    for (Player p = 0; p < 4; ++p) {
      hands[p]=swapSuitWithTrumps(hands[p],trump);
    }
  }
  return State(hands,played,useTrump,1,1);
}

State createTrickyGame(){
  Card played[4] = {0,0,0,0};
  Cards hands[4];
  hands[0] = getCards({heartjack ,heartnine ,            spadeking,                       diamondqueen,diamondjack ,clubmanil,clubace,clubking});
  hands[2] = getCards({heartace  ,                       spadejack ,spadenine ,           diamondnine ,diamondeight,clubqueen,clubjack,clubnine});
  hands[3] = getCards({heartking ,hearteight,            spadeace  ,spadeeight,           diamondace  ,diamondseven,clubeight,clubseven});
  hands[1] = getCards({heartmanil,heartqueen,heartseven, spademanil,spadequeen,spadeseven,diamondmanil,diamondking});
  return State(hands,played,true,1,1);
}

Card pickSimpleAI(Cards toPickFrom) {
  return getHighest(toPickFrom); // TODO: extend with "teamLeads"
}

double getElapsedSeconds(clock_t &begin) {
  clock_t end = clock();
  return double(end - begin) / CLOCKS_PER_SEC;
}

struct CardScore {
  Card crd = 0;
  int score = 0;
};

string cardscore2str(const vector<CardScore> &cs) {
  string out = "";
  for (int j = 0; j < cs.size(); ++j) {
    out += card2str(cs[j].crd) + ":" + to_string(cs[j].score) + " ";
  }
  return out;
}

void repairDominatedCards(Cards cs, State& game, vector<CardScore>& scores){
  assert(nextPlayer(game.getCurrentPlayer())!=game.getStartingPlayer());
  int decrement = game.getCurrentTeam()==0?-1:1;
  Cards leftInGame = game.getCardsInGame();
  // NOTE: leftInGame should include the cards currently played!
  // This is achieved by only removing played cards from hands at the end of a round (see playCard() )
  for (Suit s = 0; s < 4; ++s) { // iterate over all suits
    if(isEmpty(getCardsOfSuit(cs,s))) continue;
    bool hasDominated = false;
    Card dominating = 32;
    for(int v=7;v>=0; --v){
      Card c = getCard(s,v);
      if(!hasCard(leftInGame,c)) continue;
      if(hasCard(cs,c)){
        if(hasDominated){ // passed dominating card, so erase it
          cs = removeCard(cs,c);
          for(int i=0; i<scores.size(); ++i){
            if(scores[i].crd==dominating){
              scores.push_back({c,scores[i].score+decrement});
              break;
            }
          }
        }else{
          State clone = game;
          clone.playCard(c);
          if(clone.currentTeamAlwaysLoses()){
            hasDominated = true; // this is a dominating card
            dominating = c;
          }else{
            break; // no dominating card exists
          }
        }
      }else{
        hasDominated = false; // a gap in cs, so need to find a new dominating card
      }
    }
    hasDominated = false;
    for(int v=0;v<8; ++v){
      Card c = getCard(s,v);
      if(!hasCard(leftInGame,c)) continue;
      if(hasCard(cs,c)){
        if(hasDominated){ // passed dominating card, so erase it
          cs = removeCard(cs,c);
          for(int i=0; i<scores.size(); ++i){
            if(scores[i].crd==dominating){
              scores.push_back({c,scores[i].score+decrement});
              break;
            }
          }
        }else{
          State clone = game;
          clone.playCard(c);
          if(clone.currentTeamAlwaysWins()){
            hasDominated = true; // this is a dominating card
            dominating = c;
          }else{
            break; // no dominating card exists
          }
        }
      }else{
        hasDominated = false; // a gap in cs, so need to find a new dominating card
      }
    }

    Card zeven = getCard(s, seven);
    Card acht = getCard(s, eight);
    Card negen = getCard(s, nine);
    if (hasCard(cs, negen) && hasCard(cs, acht)) {
      for (int i = 0; i < scores.size(); ++i) {
        if (scores[i].crd == negen) {
          scores.push_back({acht, scores[i].score + decrement});
          break;
        }
      }
    }
    if (hasCard(cs, acht) && hasCard(cs, zeven)) {
      for (int i = 0; i < scores.size(); ++i) {
        if (scores[i].crd == acht) {
          scores.push_back({zeven, scores[i].score + decrement});
          break;
        }
      }
    }
    if (hasCard(cs, negen) && hasCard(cs, zeven) && !hasCard(leftInGame, acht)) {
      for (int i = 0; i < scores.size(); ++i) {
        if (scores[i].crd == negen) {
          scores.push_back({zeven, scores[i].score + decrement});
          break;
        }
      }
    }
  }
}

vector<CardScore> getScoresForMoves(State &game) {
  vector<CardScore> out;
  out.reserve(8);
  Cards current = game.getCurrentlyPlayableCards();
  Cards cleaned = cleanDominatedCards(current, game).cleaned;
  vector<Card> toPlay = getVector(cleaned);
  for (auto c: toPlay) {
    State clone = game;
    clone.playCard(c);
    out.push_back({c,pvs(clone, -1000, 1000)});
  }
  repairDominatedCards(current,game,out);
  // insertion sort from high to low
  for (int i = 0; i < out.size() - 1; ++i) {
    for (int j = i + 1; j < out.size(); ++j) {
      if (out[i].score < out[j].score || (out[i].score == out[j].score && out[i].crd < out[j].crd)) { // swap i and j
        auto tmp = out[i];
        out[i] = out[j];
        out[j] = tmp;
      }
    }
  }
  cout << cardscore2str(out) << (game.getTrumpUsed() ? "+" : "o") << endl;
  assert(out.size() == getCount(current));
  return out;
}

Card getBestCard(vector<CardScore> &cs, bool maximizing = true) {
  int factor = maximizing ? 1 : -1;
  Card bestCard = 32;
  int bestScore = -1000;
  for (int i = 0; i < cs.size(); ++i) {
    int newscore = factor * cs[i].score;
    if (newscore > bestScore) {
      bestScore = newscore;
      bestCard = cs[i].crd;
    }
  }
  return bestCard;
}

/**
 * A feature vector of 12 features
 * The first eight features give the total amount of manils, aces, kings etc.
 * The last four features give the total score of "lost" cards, which are low cards without a high card to back them up.
 * E.g., the case of a "loose ace" is captured in there, as well as a king with only one small card, etc.
 */

const uint32_t NBFEATURES = 12;
struct Features {
  int64_t f[NBFEATURES]={0,0,0,0,0,0,0,0,0,0,0,0};
};

Features getFeatures(Cards hand){
  Features result;
  for(Value v = 0; v<8; ++v){
    for(Suit s = 0; s<4; ++s){
      result.f[v]+=hasCard(hand,getCard(s,v));
    }
  }
  for(Suit s=0; s<4; ++s){
    Cards suited = getCardsOfSuit(hand,s);
    for(int i=0; i<4; ++i){
      Card highest = getCard(s,manil-i);
      if(isEmpty(suited) || hasCard(suited,highest)){
        suited = removeCard(suited,highest);
      }else{
        result.f[8+i]+=getScore(getLowest(suited));
        suited = removeCard(suited,getLowest(suited));
      }
    }
  }
  return result;
}

pair<Features,Features> getFeaturesWithTrumps(Cards hand, Suit trump){
  pair<Features,Features> result;
  Cards trumps = getCardsOfSuit(hand,trump);
  result.first=getFeatures(trumps);
  Cards nontrumps = setminus(hand,trumps);
  result.second=getFeatures(nontrumps);
  return result;
}

#if !defined(__GNUC__)
extern "C" { __declspec(dllexport)
#endif

Card play(
    Cards hand0, Cards hand1, Cards hand2, Cards hand3,
    Card card0, Card card1, Card card2, Card card3,
    bool trumpUsed, Suit trump,
    int startingPlayer, int currentPlayer) {
  Cards hands[4];
  hands[0]=swapSuitWithTrumps(hand0,trump);
  hands[1]=swapSuitWithTrumps(hand1,trump);
  hands[2]=swapSuitWithTrumps(hand2,trump);
  hands[3]=swapSuitWithTrumps(hand3,trump);
  Card played[4];
  played[0]=swapCardSuitWithTrumps(card0,trump);
  played[1]=swapCardSuitWithTrumps(card1,trump);
  played[2]=swapCardSuitWithTrumps(card2,trump);
  played[3]=swapCardSuitWithTrumps(card3,trump);
  State game = State(hands,played,trumpUsed,startingPlayer,currentPlayer);
  cout << game.toString() << endl;
  assert(!isEmpty(game.getCurrentlyPlayableCards()));
  vector<CardScore> toPlay = getScoresForMoves(game);
  SEED = hand0 ^ hand1 ^ hand2 ^ hand3 ^ card0 ^ card1 ^ card2 ^ card3;
  random_shuffle(toPlay.begin(), toPlay.end(),myrandom);
  return swapCardSuitWithTrumps(getBestCard(toPlay,getTeam(currentPlayer)==0),trump);
}

const double NOTRUMPWEIGHTS[NBFEATURES+1] = {
    -4.7, -4.6, -4.5, -4.3, -3.5, -2.0, 2.2, 15.2, -1.0, -0.2, 0.0, 0.0, -0.8
};

const double TRUMPWEIGHTS[NBFEATURES*2+1] = {
    6, 6.1, 6.2, 6.3, 7.1, 8.4, 10.1, 13.9, 1.2, 1.1, 0.0,0.0, 1.2, 2.0, 2.8, 3.6, 4.4, 8.1, -1.2, -0.8, 0.0,0.0,-33
};

const double TRUMPWEIGHTS2[NBFEATURES*2+1] = {
    -1.3,-1.2,-1.1,0.1,1.7,2.2,2.3,6.6,-1.5,-1.4,-1.3,0.0,-1.3,-1.2,-1.1,0.2,1.4,2.3,2,6.7,-1.6,-1.4,-0.8,0,0,
};


double getNoTrumpPrediction(Cards hand){
  double score = NOTRUMPWEIGHTS[NBFEATURES];
  auto fs = getFeatures(hand);
  for(int i=0; i<NBFEATURES; ++i){
    score+=NOTRUMPWEIGHTS[i]*fs.f[i];
  }
  return score;
}

double getTrumpPrediction(Cards hand, Suit trump){
  double score = TRUMPWEIGHTS[NBFEATURES*2];
  auto fs = getFeaturesWithTrumps(hand,trump);
  for(int i=0; i<NBFEATURES; ++i){
    score+=TRUMPWEIGHTS[i]*fs.first.f[i];
  }
  for(int i=0; i<NBFEATURES; ++i){
    score+=TRUMPWEIGHTS[i+NBFEATURES]*fs.second.f[i];
  }
  return score/2.0;
}

double getFinalTrumpPrediction(Cards hand, Suit trump){
  double score = TRUMPWEIGHTS2[NBFEATURES*2];
  auto fs = getFeaturesWithTrumps(hand,trump);
  for(int i=0; i<NBFEATURES; ++i){
    score+=TRUMPWEIGHTS2[i]*fs.first.f[i];
  }
  for(int i=0; i<NBFEATURES; ++i){
    score+=TRUMPWEIGHTS2[i+NBFEATURES]*fs.second.f[i];
  }
  return score;
}

Suit pickTrump(Cards hand){
  Suit result = 5;
  double bestscore = -1.0;
  for(Suit s=0; s<4; ++s){
    double score = getTrumpPrediction(hand,s);
    if(score>bestscore){
      bestscore=score;
      result=s;
    }
  }
  return result;
}

bool noTrump(Cards hand){
  Suit wouldPick = pickTrump(hand);
  double trumpscore = getFinalTrumpPrediction(hand,wouldPick);
  double notrumpscore = getNoTrumpPrediction(hand);
  return notrumpscore > trumpscore;
}

bool tap(Cards hand, bool trumpUsed, Suit trump){ // TODO: improve
  if(!trumpUsed){
    return getTrumpScore(hand) > 600;
  }else{
    return getTrumpScore(hand) > 400 && getCount(getCardsOfSuit(hand,trump))>3;
  }
}

#if !defined(__GNUC__)
}
#endif

void generateDataNoTrump(int64_t samplesize){
  for(int64_t i=0; i<samplesize; ++i) {
    State game = createRandomGame(i);
    cout << pvs(game, -1000, 1000);
    for(int k=0; k<4; ++k) {
      Features fs = getFeatures(game.getPlayerHand(k));
      for (int j = 0; j < NBFEATURES; ++j) {
        cout << " " << fs.f[j];
      }
    }
    cout << endl;
  }
}

void generateDataTrump(int64_t samplesize){
  for(int64_t i=0; i<samplesize; ++i) {
    State game = createRandomGame(i);
    Suit trump = pickTrump(game.getPlayerHand(0));
    Card played[4] = {0,0,0,0};
    Cards hands[4];
    hands[0]=swapSuitWithTrumps(game.getPlayerHand(0),trump);
    hands[1]=swapSuitWithTrumps(game.getPlayerHand(1),trump);
    hands[2]=swapSuitWithTrumps(game.getPlayerHand(2),trump);
    hands[3]=swapSuitWithTrumps(game.getPlayerHand(3),trump);
    State clone = State(hands,played,true,1,1);
    cout << pvs(clone, -1000, 1000)/2.0;
    for(int k=0; k<4; ++k){
      pair<Features,Features> fs = getFeaturesWithTrumps(game.getPlayerHand(k),hearts);
      for(int j=0; j<NBFEATURES; ++j){
        cout << " " << fs.first.f[j];
      }
      for(int j=0; j<NBFEATURES; ++j){
        cout << " " << fs.second.f[j];
      }
    }
    cout << endl;
  }
}

int main() {
//  generateDataNoTrump(100000);
//  return 0;


  State tricky = createTrickyGame();
  int64_t nodesused = TOTALNODES;
  clock_t tmpclock = clock();
  int score = pvs(tricky,-1000,1000);
  nodesused = TOTALNODES - nodesused;
  double timeused = getElapsedSeconds(tmpclock);
  std::cout << "Score: " << score << endl;
  std::cout << "time elapsed: " << timeused << " for " << nodesused << " nodes." << endl;

  int64_t nbGames = 1000;
  int64_t nbNontrumpGames = 0;
  int64_t mostNodes = 0;
  int64_t mostIndex = 0;
  double longestTime = 0;
  int64_t longestIndex = 0;

  double nontrumpNodes = 0;

  for (int64_t i = 0; i < nbGames; ++i) {
    State game = createRandomGameWithTrumpDecided(i);
    cout << "[" << i << "] ";

    int64_t nodesused = TOTALNODES;
    clock_t tmpclock = clock();
    auto toPlay = getScoresForMoves(game);
    nodesused = TOTALNODES - nodesused;
    double timeused = getElapsedSeconds(tmpclock);
    std::cout << "time elapsed: " << timeused << " for " << nodesused << " nodes." << endl;

    if (!game.getTrumpUsed()) {
      ++nbNontrumpGames;
      nontrumpNodes += nodesused;
    }
    if (mostNodes < nodesused) {
      mostNodes = nodesused;
      mostIndex = i;
    }
    if (longestTime < timeused) {
      longestTime = timeused;
      longestIndex = i;
    }
  }
  cout << "Final time elapsed: " << getElapsedSeconds(BEGIN) << " for " << TOTALNODES << " nodes." << endl;
  cout << "Nodes no trump: " << nontrumpNodes << " in " << nbNontrumpGames << " games. Average: "
       << nontrumpNodes / (double) nbNontrumpGames << endl;
  cout << "Nodes trump: " << TOTALNODES - nontrumpNodes << " in " << nbGames - nbNontrumpGames << " games. Average: "
       << (TOTALNODES - nontrumpNodes) / (double) (nbGames - nbNontrumpGames) << endl;
  cout << "Biggest game is " << mostIndex << " with " << mostNodes << " nodes." << endl;
  cout << "Longest game is " << longestIndex << " with " << longestTime << " seconds." << endl;

  return 0;
}
